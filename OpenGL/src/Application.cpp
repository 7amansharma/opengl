#include "GL/glew.h"
#include <GLFW/glfw3.h>


#include "Renderer.h"
#include "IndexBuffer.h"
#include "VertexBuffer.h"
#include "VertexArray.h"
#include "Shader.h"

int main(void)
{
	GLFWwindow* window;

	/* Initialize the library */
	if (!glfwInit())
		return -1;

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	GLenum err = glewInit();

	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		std::cout << "Error!" << std::endl;
	}
	std::cout << glGetString(GL_VERSION)<< std::endl;
	{
		float positions[] = {
			-0.5f, -0.5f,
			0.5f, -0.5f,
			0.5f,  0.5f,
			-0.5f,  0.5f,
		};

		unsigned int indices[] = {
			0,1,2,
			2,3,0
		};

		VertexBuffer vb(positions, 4 * 2 * sizeof(float));

		IndexBuffer ib(indices, 6);
		VertexBufferLayout layout;
		layout.Push<float>(2);
		VertexArray va;
		va.addBuffer(vb, layout);
		ib.Bind();

		//glEnableVertexAttribArray(0);
		//glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, 0);

		Shader shader("res/shaders/Basic.shader");
		shader.Bind();
//		ShaderProgramSource shaderSource = ParseShader("res/shaders/Basic.shader");



//		unsigned int program = CreateShader(shaderSource.VertexSource, shaderSource.FragmentSource);
//		glUseProgram(program);

//		int location = glGetUniformLocation(program, "u_Color");


		float r = 0.0f;
		float increment = 0.05f;
		while (!glfwWindowShouldClose(window))
		{
			/* Render here */
			glClear(GL_COLOR_BUFFER_BIT);
			//glDrawArrays(GL_TRIANGLES, 0, 6);
			shader.SetUniform4f("u_Color", r, 0.4, 0.7, 1.0);
			GLCALL(glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr));
			/* Swap front and back buffers */
			if (r < 0.0) {
				increment = 0.05f;
			}
			else if (r > 1.0) {
				increment = -0.05f;
			}
			r += increment;
			glfwSwapBuffers(window);


			/* Poll for and process events */
			glfwPollEvents();
		}
	}
	glfwTerminate();
	return 0;
}