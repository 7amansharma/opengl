#include "Shader.h"

Shader::Shader(const std::string& filepath):
	m_FilePath(filepath),m_RendererID(0)
{
	ShaderProgramSource shaderSource = ParseShader(filepath);
	m_RendererID = CreateShader(shaderSource.VertexSource, shaderSource.FragmentSource);

}

Shader::~Shader()
{
	GLCALL(glDeleteProgram(m_RendererID));
}

void Shader::Bind() const
{
	GLCALL(glUseProgram(m_RendererID));
}

void Shader::UnBind() const 
{
	GLCALL(glUseProgram(0));
}

ShaderProgramSource Shader::ParseShader(std::string filePath)
{
	std::ifstream stream(filePath);
	enum class shaderType {
		NONE = -1, VERTEX = 0, FRAGMENT = 1
	};
	std::stringstream ss[2];
	shaderType type = shaderType::NONE;
	std::string line;
	while (getline(stream, line)) {
		if (line.find("#shader") != std::string::npos) {
			if (line.find("vertex") != std::string::npos) {
				type = shaderType::VERTEX;
			}
			else if (line.find("fragment") != std::string::npos) {
				type = shaderType::FRAGMENT;
			}
		}
		else {
			ss[(int)type] << line << "\n";
			;
		}
	}

	return { ss[0].str(), ss[1].str() };
}

int Shader::CompileShader(unsigned int type, const std::string& shaderSource)
{
	unsigned int id = glCreateShader(type);
	const char* src = shaderSource.c_str();
	glShaderSource(id, 1, &src, nullptr);
	glCompileShader(id);

	// TODO: Error handling
	int result;
	glGetShaderiv(id, GL_COMPILE_STATUS, &result);

	if (result == GL_FALSE) {
		int length;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
		char* message = (char*)alloca(length * sizeof(char));
		glGetShaderInfoLog(id, length, &length, message);
		std::cout << "Failed to compile" << (type == GL_VERTEX_SHADER ? "vertex" : "shader") << std::endl;
		std::cout << message << std::endl;
		glDeleteShader(id);
		return 0;
	}

	return id;
}

unsigned int Shader::CreateShader(const std::string& vertexShader, const std::string& fragmentShader)
{
	unsigned int program = glCreateProgram();
	unsigned int vs = CompileShader(GL_VERTEX_SHADER, vertexShader);
	unsigned int fs = CompileShader(GL_FRAGMENT_SHADER, fragmentShader);
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);
	glValidateProgram(program);

	glDeleteShader(vs);
	glDeleteShader(fs);
	return program;
}


int Shader::GetUniformLocation(std::string name) {
	if (m_UniformLocationCache.find(name) != m_UniformLocationCache.end())
		return m_UniformLocationCache[name];
	GLCALL(int location = glGetUniformLocation(m_RendererID, name.c_str()));
	if (location == -1)
		std::cout << "Uniform not found" << std::endl;
	m_UniformLocationCache[name] = location;
	return location;
}
void Shader::SetUniform4f(std::string name, float v0, float v1, float v2, float v3) {
	GLCALL(glUniform4f(Shader::GetUniformLocation(name), v0, v1, v2, v3));
}



