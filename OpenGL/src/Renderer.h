#pragma once
#include "GL/glew.h"



#define ASSERT(x) if (!(x)) __debugbreak();
#define GLCALL(x) GLClearError(); x;ASSERT(GLLogCall(#x, __FILE__, __LINE__))

void GLClearError();
bool GLLogCall(const char* funcName, const char* fileName, int line);