#include "Renderer.h"
#include <iostream>


void GLClearError() {
	while (glGetError() != GL_NO_ERROR);
}

bool GLLogCall(const char* funcName, const char* fileName, int line) {
	while (GLenum error = glGetError()) {
		std::cout << "[OpenGL Error] " << funcName << " " << " " << fileName << " " << line << " (" << error << ")" << std::endl;
		return false;
	}
	return true;
}
