#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include "Renderer.h"
#include <unordered_map>
struct ShaderProgramSource {
	std::string VertexSource;
	std::string FragmentSource;
};

class Shader
{
private:
	unsigned int m_RendererID;
	std::string m_FilePath;
	std::unordered_map<std::string, int> m_UniformLocationCache;
public:
	Shader(const std::string& filename);
	~Shader();

	void Bind() const;
	void UnBind() const;
	void SetUniform4f(std::string name, float v0, float v1, float v2, float v3);
private:
	ShaderProgramSource ParseShader(std::string filePath);
	int CompileShader(unsigned int type, const std::string& shaderSource);
	unsigned int CreateShader(const std::string& vertexShader, const std::string& fragmentShader);
	int GetUniformLocation(std::string name);

};

